import { useEffect } from "react";
import { useParams } from "react-router";
import { useState } from "react";
import { Link } from "react-router-dom";

const List = (props) => {
  const { id } = useParams(0);
  const [data, setData] = useState({ address: {} });
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        setData(result[id - 1]);
      });
    console.log(id);
  }, []);

  return (
    <div>
      <h1 style={{ width: 600, margin: 10 }}>User Lists</h1>
      <div style={{border: "1px solid black", width: 600, margin: 10, padding: 10,}}>
        <p>ID : {data.id}</p>
        <p>Name : {data.name}</p>
        <p>Email : {data.email}</p>
        <p>
          Adress : {data.address.street} {data.address.suite} {data.address.city}
        </p>
        <Link to={`/user-more/${id}`}>
          <button style = {{ marginLeft: 500, backgroundColor: "deepskyblue", color: "white" }}>
              View More
          </button>
        </Link>
      </div>
    </div>
  );
};

export default List;