import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import { useEffect } from "react";

const List = ({ data }) => {
  const showData = () => {
    console.log(data);
    alert(JSON.stringify(data));
  };
  return (
    <div style={{ border: "1px solid black", width: 600, margin: 10, padding: 10 }}>
      <p>ID : {data.id}</p>
      <p>Name : {data.name}</p>
      <p>Email : {data.email}</p>
      <p>
        Address :{" "}
        {`${data.address.street} ${data.address.suite} ${data.address.city}`}
      </p>
      <button
        onClick={showData}
        style={{ marginLeft: 500, backgroundColor: "deepskyblue", color: "white" }}
      >
        View More
      </button>
    </div>
  );
};

export default List;
