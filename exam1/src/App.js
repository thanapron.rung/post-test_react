import logo from './logo.svg';
import './App.css';
import { useState } from "react";
import { useEffect } from "react";
import List from './list';

// ธนพร รุ่งวิทยานนท์

function App() {
  const [list, setList] = useState([]);
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
    .then((res) => res.json())
    .then((result) => {
      console.log(result);
      setList(result);
    });
  }, []);
  return (
    <div>
      <h1 style={{ width: 600, margin: 10 }}>User Lists</h1>
      {list.map((data) => (
        <List data = {data} />
      ))}
    </div>
  );
}

export default App;
